# Mermaid support

Install Markdown Preview Mermaid Support plugin VSCode plugin

# Architecture

The architecture consists of several Layers:

* Vending Machines - this a Vending Machin Fleet.
* Payment Layer - Payment Layer runs PaymentGateway Service and each Vending Machine connects to it when a customer pays with credit card.
* Data Layer - Data Layer uses mongoDB as operational database to store transactions, PostgreSQL - to store all item information and their relations to each other (Neo4J could be an alternative to PostgreSQL as it is more suited to this kind of data organization)
* Monitor Layer - Monitor Layer has two components. One is a keymetrics service that have all the metrics published to it (by the means of **pmx**). The second one is Syslog collector (any service such as Splunk that can collect logs from each service)
* AWS Cloud - AWS services are used store logs, metrics and transactions for analytics purposes. The **Ingest Service** continuously extracts the data from all relevant sources and publishes it to Kinesis. Kinesis has several lambdas connected to it and each event launches relevant lambda that can then process the data and insert it into corresponding data store.



```mermaid
graph TB
subgraph Vending Machines
V1(Vending Machine1)
V2(Vending Machine2)
end

subgraph Data Layer
M(MongoDB)
PG(PostgreSQL)
V1-->|place tx|M
V2-->|place tx|M
end

subgraph Payment Layer
PGW(PaymentGateway Service)
V1-->|place payment|PGW
V2-->|place payment|PGW
PGW-->|Update Transaction|M
end

subgraph Monitor Layer
S[Syslog collector]
K[Keymetrics]

S-->|syslog collect|V1
S-->|syslog collect|V2


K-->|Collect PMX Metrics from <br> all Vending Machines|V1
end

subgraph AWS Cloud
LM(Metrics Lambda)
LL(Logs Lambda)
LA(Analytics Lambda)
KS[Kinesis]
KSI[Ingest Service]
RS[RedShift / Dynamo / ES]
ES[Elastisearch]
CW[CloudWatch]

S-.Publish to Log stream.->KSI
K-.Publish to Metrics Stream.->KSI
M-.Publish to Analytics Stream.->KSI

KSI-->KS

KS-->LL
KS-->LM
KS-->LA


LL-->|Store Logs|ES
LM-->|Publish|CW
LA-->|Store Transactions|RS
end

style V1 fill:orange,stroke:#333,stroke-width:2px
style V2 fill:orange,stroke:#333,stroke-width:2px
style M fill:lightseagreen,stroke:#333,stroke-width:2px
style PG fill:lightseagreen,stroke:#333,stroke-width:2px
style S fill:lightsteelblue,stroke:#333,stroke-width:2px
style PGW fill:coral,stroke:#333,stroke-width:2px
style KSI fill:lightcoral,stroke:#333,stroke-width:2px
```

# Sequence diagram

This sequnce diagram show the flow of a split payment with cancellation

```mermaid

sequenceDiagram

    User->>ItemManager: Chooses Item 

    User->>ItemManager: Places 50/50
    ItemManager->>PaymentProxy: place 50% coins
    ItemManager->>PaymentProxy: place 50% credit

    PaymentProxy->>CoinPayment: Place 50%
    loop Collection loop
        PaymentProxy->>PaymentProxy: Pending completion
    end
    PaymentProxy->>CreditPayment: Place 50%
    CreditPayment->>TransactionManager: TX status - placed
    CreditPayment->>PaymentProcessot: Place order

    loop Approval loop
        CreditPayment->>TransactionManager: TX status - pending approval
    end

    User->>ItemManager: Cancels transaction
    ItemManager->>PaymentProxy: Cancel TX

    PaymentProxy->>CreditPayment: Transaction canceled
    PaymentProxy-->CoinPayment: Cancel
    CoinPayment->>User: Return coins
    CreditPayment->>TransactionManager: TX status - cancelled
    CreditPayment->>PaymentProxy: Notify Cancellation
    PaymentProxy->>ItemManager: Notify cancellation
    ItemManager->>User: Notify cancellation

```