export class CoinPayment {

    public sum;  
    public loadedSum;
    
    /**
     * 
     * @param sum - this how many we need to pay for all items
     * @param loadedSum - this how much was loaded into slots
     */
    constructor({sum, loadedSum}:{sum: number, loadedSum: number}) {
        this.loadedSum  = loadedSum;
        this.sum        = sum;
    }

    async process() {
    }

    calculateChange(total: number) {
        return this.loadedSum - this.sum;
    }

    async cancel() {
        // This will return all the change to the customer
        return true;
    }
}