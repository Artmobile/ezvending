import { CoinPayment }      from './coin-payment';
import { CreditPayment }    from './credit-payment';
import { STATUS_CODES }     from 'http';
import { Transaction }      from '../transaction';     
import { Item }             from '../item'; 

export class PaymentProxy {

    private coinPayment:    CoinPayment;
    private creditPayment:  CreditPayment
    private change:         number; // This change will be returned to the user in the end of operation

    constructor(creditPayment, coinPayment) {
        this.coinPayment = coinPayment;
        this.creditPayment = creditPayment;
    }

    async process(items: Item[], total): Promise<Transaction> {

        // If pay with Cash, we want to check if there is any change we should be returning
        const change = this.coinPayment ? this.coinPayment.calculateChange(total) : 0;
        const tx = new Transaction(items);
        tx.change = change;
        if(!this.creditPayment) {
            // No split payments - just return the change
            // If the number is negative, the vending machine will ask for more coins or
            // offer to pay with credit card. If it is possible, it will return some coins 
            // to the customer, otherwise return everything and keep the merchandaise
            return tx;
        } 

        // In case of cancellation, the creditPayment will throw EZVTransactionCanceled
        // Note that await here will wait not for transaction completion but rather
        // until transaction is mapped (in Redis, for example) for future lookups
        await this.creditPayment.process(tx);

        // From this point on check status for the transaction
        return tx;
    }

    async cancel() {
        return await Promise.all([this.coinPayment.cancel, this.creditPayment.cancel]);
    }

}