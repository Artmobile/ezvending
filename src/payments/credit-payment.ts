import { Transaction, TRANSACTION_STATUS } from '../transaction';
import { TransactionManager } from '../transaction-manager';

export class CreditPayment {
    public sum:         number;
    public total:       number;

    public transaction: Transaction;
    public transactionManager: TransactionManager;

    constructor( sum: number, transactionManager: TransactionManager ) {
        this.sum = sum;
    }

    async process(tx: Transaction) {
        this.transaction = tx;

        // Now we can send transaction to the PaymentGatewayService
        // await axios.post('http://pgw.ezvending.com/process', {data: tx})
    }

    async poll() {
        return new Promise((res, rej)=> {
            setTimeout(async () => {
                // await axios.post('http://pgw.ezvending.com/status', {data: tx.transactionId})
                if(true) { 
                    this.transaction.status = TRANSACTION_STATUS.DONE;
                    await this.transactionManager.update(this.transaction);
                    return res();
                }
                // rej()
            }, 500)
        })
    }

    async cancel() {
        // This will send cnacellation request to the Payment Processor
        return true;
    }

}