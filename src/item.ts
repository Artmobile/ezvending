/**
 * This class is used to calculate Item price. It considers in account that there may be
 * subitems on this item with different configuration
 */
export class Item {

    public subitems:    Item[];
    public price:       number;
    public name:        string;
    public count:       number;

    total() {
        return this.price + this.calculateSubitemTotal();
    }


    /**
     * Calculation of subitems can be tricky. It assumes that subitems have multiple counts
     * for the SAME configuration. Variation in configurations will produce a different subitem
     * For example, 3 x [Machiato without sugar] and 4 x [Machiato with sugar] will produce two 
     * different subitems even though <Machiato> item is the same
     */
    calculateSubitemTotal() {

        if(!this.subitems || this.subitems.length === 0) {
            return 0;
        }

        let total = 0;

        this.subitems.forEach((subitem: Item) => {
            total += (subitem.total() * subitem.count);
        })

        return total;
    }
}