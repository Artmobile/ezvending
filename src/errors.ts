export class EZVError extends Error {
    public innerError: any;
    constructor(message: string) {
        super(message);
    }

    setInnerError(err): EZVError {
        this.innerError = err;
        return this;
    }
}

export class EZVPendingTransaction extends EZVError {
    constructor() {
        super('Cannot Add more items because of a pending transaction')
    }
}

export class EZVTransactionFailed extends EZVError {
    public transactionId: string;
    constructor(transactionId) {
        super(`The transaction failed. TRansaction id: ${transactionId}`)
        this.transactionId = transactionId;
    }
}