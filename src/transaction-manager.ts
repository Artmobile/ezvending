import { promisify }    from 'util';
import * as Datastore   from 'nedb';
import { Transaction }  from './transaction';

export class TransactionManager {

    // Mock MongoDB to keep transactions
    private db = new Datastore({inMemoryOnly: true})
    

    constructor() {
        this.db.loadDatabase();
        this.db.ensureIndex({fieldName: 'transactionId'});
    }

    async update(tx:Transaction) {
        return new Promise((res, rej) => {
            this.db.update({ transactionId: tx.transactionId }, tx, {}, (err, t) => {
                if(err) return rej(err);
                res(t);
            })
        });
    }


    async insert(tx:Transaction) {
        return new Promise((res, rej) => {
            this.db.insert(tx, (err, t) => {
                if(err) return rej(err);
                res(t);
            })
        });
    }

    async find(transactionId: string) {
        return new Promise((res, rej) => {
            this.db.find({transactionId}, (err, t) => {
                if(err) return rej(err);
                res(t);
            })
        });
        
    }
    

}