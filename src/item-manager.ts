import { Readable }             from 'stream';
import * as pmx                 from 'pmx';

import { EZVPendingTransaction, EZVTransactionFailed }    from './errors'
import { Item }                 from './item';

import { 
    CreditPayment, 
    PaymentProxy, 
    CoinPayment }               from './payments';
import { Transaction, TRANSACTION_STATUS }          from './transaction';
import { TransactionManager }   from './transaction-manager';

/**
 * ItemManager is created for each purchase. It will keep track of items, 
 * communicate with PaymentProxy and with Vending machine (not shown in the excercise)
 */

export class ItemManager extends Readable {
    
    private paymentProxy:       PaymentProxy;
    private pendingItems:       Item[];
    private transactionManager: TransactionManager;
    private currentTx:          Transaction;

    private processorTimer:     NodeJS.Timer;

    constructor(paymentProxy: PaymentProxy, transactionManager: TransactionManager ) {
        super();
        this.paymentProxy       = paymentProxy;
        this.transactionManager = transactionManager;
    }

    _read(size: number) {

        // Normally would go to creditPayment.poll()
        // But for the things to tick we just simulate it with
        // setTimeout
        this.processorTimer = setTimeout(async () =>{

        const foundTx = await this.transactionManager.find(this.currentTx.transactionId);

        // Update transaction status
        this.currentTx.status = TRANSACTION_STATUS.DONE;
        await this.transactionManager.update(this.currentTx);
            this.emit('data', this.currentTx);
        }, 3000);
    }

    /**
     * Returns a list of all available items
     */
    displayAvailableItems() {

    }

    /**
     * Display all items that are currently being purchased
     */
    displayPendingItems() {
    }


    /**
     * This method assumes that the vending machine recieved full payment
     * either in coins, credit or split
     * @param {*} items An array of items
     */
     async purchase(items: Item[]): Promise<ItemManager> {

        // Here we can check if there any pending items 
        // and decide what to do in this situation. We may throw
        // a warning or append them to current TX, depending on 
        // design

        // Currently we just throw an error
        if(this.pendingItems && this.pendingItems.length) {
            throw new EZVPendingTransaction();
        }

        // Save pending items for later
        this.pendingItems = items;

        // Tell payment proxy to process the records. We laso provide total cost of all products
        // so that Payment objects (CoinPayment and Credit payment will know how to split)
        const total = this.calculateTotal();
        let tx = null;
        try {
            tx = this.currentTx = await this.paymentProxy.process(items, total);

            // Save transaction in global lookup
            tx.status = TRANSACTION_STATUS.PENDING;
            const dbTx =  await this.transactionManager.insert(tx);
        } catch(ex) {
            this.emit('error', new EZVTransactionFailed(tx).setInnerError(ex))
        }

        return this;
    }

    /**
     * Cancels pending transaction
     */
    async cancel() {

        // Simulate processor cancelation 
        await this.paymentProxy.cancel();
        clearTimeout(this.processorTimer);

        this.currentTx.status = TRANSACTION_STATUS.CANCELED;
        await this.transactionManager.update(this.currentTx);

        this.emit('cancel', this.currentTx);
    }

    calculateTotal(): number {

        let total = 0;
        this.pendingItems.forEach((item: Item) => {
             total += (item.total());
        })

        return total;
    }


}