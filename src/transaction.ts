import * as moment      from 'moment';
import { Chance }       from 'chance';

import { Item }         from './item';
import { ItemManager }  from './item-manager'

export enum TRANSACTION_STATUS {
    CANCELED    = -2,
    FAILED      = -1,    
    NEW         = 0,
    PENDING     = 1,
    DONE        = 2
}

export class Transaction {

    public items:           Item[];
    public dtStarted:       moment.Moment;
    public transactionId:   string;
    public status:          TRANSACTION_STATUS;
    public change:          number;

    constructor(items: Item[]) {
        this.dtStarted      = moment();
        this.transactionId  = new Chance().guid(); 
        this.status         = TRANSACTION_STATUS.NEW;
        this.items          = items;
    }

    public commit() {
        this.status         = TRANSACTION_STATUS.DONE;
    }

    public rollback() {
        // Here we can also goto the database and rollback all pending DB transaction
        // or delete/change relevant records
        this.status         = TRANSACTION_STATUS.CANCELED;
    }
}