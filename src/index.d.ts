declare namespace NodeJS {

    // Augment Process to have additional properties
    interface Process {
        // Used fin shutdown.js. Otherwise typescript throws:
        // Error:(81, 13) TS2345:Argument of type 'string' is not assignable to parameter of type 'Signals'.
        // Alos, note that process.on has sig: string, not sig:Signals so that this particular
        // overload of on is used by typescript.
        probe: any
    }
}
