import * as path            from 'path';
import { Readable }         from 'stream';
import * as pmx             from 'pmx';


import { ItemManager }      from './item-manager';
import { Item }             from './item';
import { 
    CoinPayment, CreditPayment, PaymentProxy
}      from './payments';

import { Transaction }      from './transaction';
import { TransactionManager} from './transaction-manager';

// Load data from a mock store
// It simulates what user chose from the vending machine
const ITEMS = require(path.join(__dirname,'mock-data/items.json'));
let 
    txManager:      TransactionManager, 
    paymentProxy:   PaymentProxy,
    itemManager:    ItemManager,
    tx:             Transaction,
    items:          Item[],
    payingInCredit: number,
    payingInCoins:  any;


// Metrics will be published to keymetrcis
process.probe = pmx.probe();    

class VendingMachine {
    async placeOrder (items, payInCredit: number, payingInCoins: {sum: number, loadedSum: number } ): Promise<Readable> {

        // There is only one Transaction manager because we assume that Vending Machine can process
        // one item at a time. In case if there can be more than one transaction in parallel,
        // Transaction manager should be made global and be able to handle multiple transactions
        txManager = new TransactionManager();
    
        paymentProxy = new PaymentProxy(
            new CreditPayment(payingInCredit, txManager), 
            new CoinPayment(payingInCoins)
        );
    
        itemManager = new ItemManager(paymentProxy, txManager);
    
        // For the sake of this excercise assuming that we can only have on level of subitems
        // In case of multiple levels - consider going recursive
        items = ITEMS.items.map((i) => {
    
            // Scaffold top level item
            const item = Object.assign(new Item(), i)
    
            // Scaffold subitems
            item.subitems = item.subitems.map((s) => {
                return Object.assign(new Item(), s);
            });
            return item;
        })

        return await itemManager.purchase(items)
    } 
}

(async () => {

    try {

        // Create an instance of vending machine
        const vendingMachine = new VendingMachine();

        process.probe.metric({
            name    : 'TXITEMCOUNTER',
            value() {
                return ITEMS.length
            }

        });

        // Place an order. It will be procressed for hardcorded 3 seconds
        (await vendingMachine.placeOrder(items, 230, {sum: 38, loadedSum: 100}))
            .on('error', (err) => {
                console.log('Error ocurred while processing transaction');
                process.probe.metric({
                    name    : 'TXERROR',
                  });
            })
            .on('cancel', (tx) => {
                console.log('The transaction has been canceled')
                process.probe.metric({
                    name    : 'TXCANCEL',
                  });

            })
            .on('data', (tx) => {
                console.log('The transaction has been completed')
                process.probe.metric({
                    name    : 'TXOK',
                  });

            });

        // // Uncomment this block to simulate cancellation after 1 second            
        // setTimeout(() => {
        //     itemManager.cancel();
        // }, 1000);    

    } catch (ex) {
        // Something wrong happened. In this excercise, just print the error to console
        console.error(ex);
    }
})();